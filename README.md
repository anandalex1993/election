# Election Dapp

The Government of India (GOI) along with the Election Commission of India(ECI) is planning to conduct the Lok Sabha elections for a particular constituency on a private ethereum blockchain. 
For every registered voter, there will be a separate smart contract. 
The access to this smart contract is only available with the citizen’s address. 
The citizen contract can only be accessed by members of ECI. The citizen can allow any other third party (exit polling organisations) if they wish to.

hosted @ https://election-contract-dapp.herokuapp.com/
