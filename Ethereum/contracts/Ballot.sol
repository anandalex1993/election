// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.7.0;

contract VoterList {
    //List of all registered voters
    struct VotersListStruct {
        uint256 voterCount;
        mapping(uint256 => address) voterslist;
    }

    VotersListStruct registeredVoter;

    mapping(address => address) citizenToContract; //Mapping of Citizen address to citizen contract address.

    constructor(address[] memory _voters, address[] memory _eciMembers) {
        for (uint256 i = 0; i < _voters.length; i++) {
            registeredVoter.voterCount++;
            registeredVoter.voterslist[registeredVoter.voterCount] = _voters[i];

            Citizen newCitizen = new Citizen(_voters[i], _eciMembers);
            citizenToContract[_voters[i]] = address(newCitizen);
        }
    }

    /**
     * Get all voters: input none,returns list of Users address
     * request null
     * response address[]
     */
    function getAllVoters() internal view returns (address[] memory) {
        address[] memory allVoters = new address[](registeredVoter.voterCount);
        uint256 j = 1;
        for (uint256 i = 0; i < registeredVoter.voterCount; i++) {
            allVoters[i] = registeredVoter.voterslist[i + j];
        }

        return allVoters;
    }

    /**
     * Get contract address: input (user address) returns users contract address
     * request address
     * response address
     */
    function getContractAddress(address _userAddress)
        internal
        view
        returns (address)
    {
        return citizenToContract[_userAddress];
    }

    /**
     * Get user details: input (contract address) returns user details
     * Request _contractAddress
     * Response voter details
     */
    function getUserDetails(address _contractAddress)
        internal
        view
        returns (
            string memory,
            uint8,
            Citizen.voterGender,
            address
        )
    {
        Citizen citizenContract = Citizen(_contractAddress);
        return (
            citizenContract.name(),
            citizenContract.age(),
            citizenContract.gender(),
            citizenContract.voterAddress()
        );
    }

    /**
     * isRegistered: input none, returns true or false depending on whether the querying user is registered.
     * Request null
     * Response bool
     */
    function isRegistered(address _user) internal view returns (bool) {
        for (uint256 i = 1; i <= registeredVoter.voterCount; i++) {
            if (_user == registeredVoter.voterslist[i]) {
                return true;
            }
        }
        return false;
    }
}

contract Ballot is VoterList {
    uint8 candidateCount = 1;
    mapping(uint256 => address) candidates; // List of candidates
    uint256 votingTimePeriod; // Voting time period
    uint8 eciMembersCount = 1;
    mapping(address => uint256) eciMembers; // List of all ECI members
    struct Vote {
        uint256 candidateCount;
        mapping(uint256 => uint256) votes; // Votes each candidate has received.
    }

    Vote allVote;

    mapping(address => uint8) userToCandidate; // User address=> candidate voted mapping

    constructor(
        uint256 _votingTimePeriod,
        address[] memory _candidates,
        address[] memory _eciMembers,
        address[] memory _voters
    ) VoterList(_voters, _eciMembers) {
        votingTimePeriod = block.timestamp + _votingTimePeriod * 1 minutes;

        for (uint256 i = 0; i < _candidates.length; i++) {
            candidates[candidateCount] = _candidates[i];
            candidateCount++;
        }

        for (uint256 j = 0; j < _eciMembers.length; j++) {
            eciMembers[_eciMembers[j]] = eciMembersCount;
            eciMembersCount++;
        }

        allVote.candidateCount = _candidates.length;
    }

    modifier onlyECI() {
        require(eciMembers[msg.sender] != 0);
        _;
    }

    /**
     * Get My Vote: input none, returns candidate which the querying user has voted, false if not voted yet.
     * request null
     * response candidateId
     */
    function getMyVote() public view returns (uint8) {
        return
            userToCandidate[msg.sender] != 0 ? userToCandidate[msg.sender] : 0;
    }

    /**
     * Get Results: if the voting has ended, return the votes each candidate has received.
     * public access
     */
    function getResults() public view returns (uint256[] memory) {
        require(votingTimePeriod < block.timestamp, "!V"); //Voting not completed

        uint256[] memory voteCount = new uint256[](allVote.candidateCount);
        uint256 j = 1;
        for (uint256 i = 0; i < allVote.candidateCount; i++) {
            voteCount[i] = allVote.votes[i + j];
        }

        return voteCount;
    }

    /**
     * Cast Vote: If user in voter list, cast their vote, also update their individual contract for the same
     * ECI access only
     */
    function castVote(address _userAddress, uint8 _candidateId) public onlyECI {
        if (isRegistered(_userAddress)) {
            // check for registered voter
            if (candidates[_candidateId] != address(0)) {
                // check for candidate
                userToCandidate[_userAddress] = _candidateId; // user candidate mapping

                allVote.votes[_candidateId] = allVote.votes[_candidateId] + 1; // update candidate each candidate received

                address citizenContractAddress =
                    getContractAddress(_userAddress); // citizen contract address
                Citizen citizenContract = Citizen(citizenContractAddress);
                citizenContract.setVote(_candidateId, msg.sender); // vote casted in contract
            }
        }
    }

    /**
     * Get Users Vote: input user address, returns the candidate the user has voted, false if not voted yet, throw error if no user exists.
     * ECI access only
     */
    function getUsersVote(address _user) public view onlyECI returns (uint8) {
        address userContractAddress = getContractAddress(_user);
        require(userContractAddress != address(0), "!U"); //user doesn't exists
        Citizen citizenContract = Citizen(userContractAddress);
        return citizenContract.vote();
    }

    /**
     * Get Vote Map : input none, returns list of users and the votes they have cast.
     * ECI access only
     */
    function getVoteMap()
        public
        view
        onlyECI
        returns (address[] memory, uint8[] memory)
    {
        address[] memory citizenList = getAllVoters();

        uint8[] memory citizenVote = new uint8[](citizenList.length);
        for (uint256 i = 0; i < citizenList.length; i++) {
            citizenVote[i] = userToCandidate[citizenList[i]];
        }

        return (citizenList, citizenVote);
    }

    /**
     * Consolidate votes: Match votes from ballot contract and summation of votes in each users contracts. Basically there are two copies of the votes cast,
     * one with the users contract and one in the ballot contract. Throw an error if there are any discrepancies. Take care of users who have not voted yet
     * ECI access only
     */
    function consolidateVotes() public view onlyECI {
        address[] memory citizenList = getAllVoters();

        for (uint256 i = 0; i < citizenList.length; i++) {
            uint256 userVote = getUsersVote(citizenList[i]);

            if (userVote != 0) {
                // check voter is voted
                if (userVote != userToCandidate[citizenList[i]]) {
                    revert("!CV"); // copy dont match
                }
            }
        }
    }
}

contract Citizen {
    enum voterGender {male, female} //Gender

    string public name; //Name
    uint8 public age; //Age
    voterGender public gender; //Gender
    address public voterAddress; //Address
    uint8 public vote; //Vote given to(once vote is given)
    uint8 eciMembersCount = 1;
    mapping(address => uint256) eciMembers; // List of all ECI members

    // Permission to access contract (will be addresses of other users),
    // should also maintain the expiry of that address to the particular users contract.
    mapping(address => uint256) permissionToAccess;

    constructor(address _voterAddress, address[] memory _eciMembers) {
        voterAddress = _voterAddress;

        for (uint256 j = 0; j < _eciMembers.length; j++) {
            eciMembers[_eciMembers[j]] = eciMembersCount;
            eciMembersCount++;
        }
    }

    /**
     * eci can set vote in citizen contract
     * request vote
     * response bool
     */
    function setVote(uint8 _vote, address _eciMemeberAddress)
        external
        returns (bool)
    {
        require(eciMembers[_eciMemeberAddress] != 0, "!A"); //Unauthorised to cast vote
        if (vote == 0) {
            vote = _vote;
            return true;
        } else {
            return false;
        }
    }

    /**
     * citizen can set name age gender
     * request name,age,gender
     * throw event
     */
    function setCitizen(
        string memory _name,
        uint8 _age,
        uint256 _gender
    ) public {
        require(msg.sender == voterAddress, "!A");

        name = _name;
        age = _age;
        gender = _gender == 1 ? voterGender.male : voterGender.female;
    }

    /**
     * Get Details: input none, returns the details, check the address of msg.sender to make sure either it is the user querying the detail, or a member of the ECI or some one the user has given access
     * request null
     * response voter details
     */
    function getDetails()
        public
        view
        returns (
            string memory,
            uint8,
            voterGender,
            address,
            uint8
        )
    {
        if (msg.sender == voterAddress) {
            return (name, age, gender, voterAddress, vote);
        } else if (eciMembers[msg.sender] != 0) {
            return (name, age, gender, voterAddress, vote);
        } else if (permissionToAccess[msg.sender] != 0) {
            if (permissionToAccess[msg.sender] >= uint8(block.timestamp)) {
                return (name, age, gender, voterAddress, vote);
            } else {
                revert("Unauthorised");
            }
        } else {
            revert("Unauthorised");
        }
    }

    /**
     * Give access: input address, expiry, returns true or false of whether the permission was set. Take a default value for expiry if expiry is not provided.
     * request address, expiry
     * response bool
     */
    function giveAccess(address _permissionToAddress, uint256 _expiry)
        public
        returns (bool)
    {
        require(msg.sender == voterAddress, "!A"); //only voter allowed

        permissionToAccess[_permissionToAddress] =
            block.timestamp +
            _expiry *
            1 minutes; //setting expiry using block timestamp
        return true;
    }

    /**
     * Default value if expiry not provided using function overloading
     * request address
     * response bool
     */
    function giveAccess(address _permissionToAddress) public returns (bool) {
        if (permissionToAccess[_permissionToAddress] != 0) {
            return true;
        } else {
            permissionToAccess[_permissionToAddress] =
                block.timestamp +
                5 *
                1 minutes; // setting to default expiry
            return false;
        }
    }
}
